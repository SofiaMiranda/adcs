import math

def s_mean(ds):
	suma=0.0
	for x in ds:
		suma+=int(x)
	mean=suma/len(ds)
	return mean

def s_stddev(ds):
	sqsum=0.0
	mean=s_mean(ds)
	for x in ds:
		sq=((mean-int(x))*(mean-int(x)))
		sqsum+=sq
	stdev=math.sqrt(sqsum/len(ds))
	return stdev

def s_median(ds):
	middle=0
	s=toint(ds)
	middle = (len(ds)/2)
	med= s[middle-1]	
	return med

def n_quart(ds):
	quart=0
	n=check_num(raw_input("Which quartil: "))
	while not (n <= 4 and n>=0 and n!=False):
		print "Value must be between 0 - 4"
		n=check_num(raw_input("Which quartil: "))
		
	
	s=toint(ds)
	k=int(math.ceil(((len(ds)+1)*(n/4.0))-1))
	quart=s[k-1]	
	return quart
	
def n_perc(ds):
	perc=0.0
	n=check_num(raw_input("Which percentil: "))
	while not (n <= 100 and n>=0 and n!=False):
		print "Value must be between 0 - 100"
		n=check_num(raw_input("Which percentil: "))

	s=toint(ds)
	k=int(math.ceil((n/100.0)*len(ds)))
	perc=s[k-1]
	return perc

def toint(ds):
	#order list
	numlst=[ ]
	for x in ds:
		numlst.append(int(x))
	s=sorted(numlst)
	return s

def check_num(number):
	try:
      		return int(number)
  	except ValueError:
		print "Error, only numbers allowed"		
		return False

def openFile():
	#with open('dataset') as f:
			
	try:
		with open('dataset') as f:
			ds = f.read().splitlines()
			return ds
	except IOError:
		print "Error, file does not exist"	
		return False
	
    		
	

dataset=raw_input("Insert list or press Enter to read 'dataset' file: ")

if dataset=="":
	ds=openFile()
else:
	ds=dataset.split()

if not ds==False:
	for x in ds:
		number=check_num(x)
		if number==False:
			break
	
	if not number==False:
		mean=s_mean(ds)
		print "mean: ", mean
		stddev=s_stddev(ds)
		print "standard deviation: ",stddev
		med=s_median(ds)
		print "median: ", med
		quart=n_quart(ds)
		print "n-quartil: ",quart
		perc= n_perc(ds)
		print "n-percentil: ",perc	




