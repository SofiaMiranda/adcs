decRom = [(1000, 'M'), 
	(900, 'CM'), 
	(500, 'D'), 
	(400, 'CD'), 
	(100, 'C'), 
	(90, 'XC'),
	(50, 'L'),
	(40, 'XL'), 
	(10, 'X'),
	(9, 'IX'), 
	(5, 'V'), 
	(4, 'IV'), 
	(1, 'I')]

def decToRom(num):
    roman = ''
    while num > 0:
        for k, v in decRom:
            while num >= k:
                roman += v
                num -= k
    return roman

def check_num(number):
	try:
      		return int(number)
  	except ValueError:
		print "Error, only numbers allowed"		
		return False

num=False
while num==False:		
	num=check_num(raw_input("number you want to convert: "))
	roman=decToRom(int(num))
	print roman

	





