"""@package docstring
Documentation for this module.

More details.
"""

import unittest


class SortDataLists:
    """Documentation for a class.
    More details.
    """
    def __init__(self, path, array):
        """The constructor."""
        self.path = path
        self.array = array

    def set_input_data(self):
        """Method to set the file to input data. The file must be previously created"""
        slist = []
        try:
            with open(self.path) as f:
                lines = f.read().splitlines()
                for line in lines:
                    slist.append(line)
                return slist
                # data = f.read()
                # return data
        except IOError:
            """Exception if file do not exist"""
            return "Error, file does not exist"

    def set_output_data(self):
        """Method to set the file to output data. The file must be previously created"""
        try:
            with open(self.path) as f:
                data = f.read()
                return data
        except IOError:
            """Exception if file do not exist"""
            return "Error, file does not exist"

    def execute_merge_sort(self):
        """Method to execute merge sort"""
        i = 0
        j = 0
        k = 0

        if len(self.array) > 1:
            mid = len(self.array) // 2
            left = self.array[:mid]
            right = self.array[mid:]

            leftsort = SortDataLists("", left)
            leftsort.execute_merge_sort()
            rightsort = SortDataLists("", right)
            rightsort.execute_merge_sort()

            while i < len(left) and j < len(right):
                if left[i] < right[j]:
                    self.array[k] = left[i]
                    i += 1
                else:
                    self.array[k] = right[j]
                    j += 1
                k += 1

            while i < len(left):
                self.array[k] = left[i]
                i += 1
                k += 1

            while j < len(right):
                self.array[k] = right[j]
                j += 1
                k += 1
        return self.array


it = SortDataLists("data.csv", "")
ret = it.set_input_data()

it2 = SortDataLists("data.csv", ret)
ret3 = it2.execute_merge_sort()


class TestSortMethods(unittest.TestCase):
    """Class to test the sort methods"""
    def test_fileInputExist(self):
        """Test to check file input exists"""
        filename = "data.csv"
        t = SortDataLists(filename, "")
        ret = t.set_input_data()
        self.assertNotEqual(ret, "Error, file does not exist")

    def test_fileInputNotExist(self):
        """Test to check that file input do not exists"""
        filename = "data.csv"
        filename = filename + 'a'
        t = SortDataLists(filename, "")
        ret = t.set_input_data()
        self.assertEqual(ret, "Error, file does not exist")

    def test_fileOutputExist(self):
        """Test to check file output exists"""
        filename = "data.csv"
        t = SortDataLists(filename, "")
        ret = t.set_output_data()
        self.assertNotEqual(ret, "Error, file does not exist")

    def test_fileOutputNotExist(self):
        """Test to check that file output do not exists"""
        filename = "data.csv"
        filename = filename + 'a'
        t = SortDataLists(filename, "")
        ret = t.set_output_data()
        self.assertEqual(ret, "Error, file does not exist")

    def test_returnSomethingMergeSort(self):
        """Test to check the return result"""
        it = SortDataLists("data.csv", "")
        ret = it.set_input_data()
        it2 = SortDataLists("data.csv", ret)
        ret3 = it2.execute_merge_sort()
        self.assertNotEqual(ret3, "")


if __name__ == '__main__':
    unittest.main()
