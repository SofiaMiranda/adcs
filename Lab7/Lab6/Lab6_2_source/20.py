"""@package docstring
This module manages a directory.
"""

import unittest
import sys
import StringIO

pathd="directory.txt"

class directory:
	"""Documentation for directory class.
	This class implements methods for a directory.
	"""
	item=[]
	items=[]
		
	def __init__(self,name,email,age,country):
		self.name=name
		self.email=email
		self.age=age
		self.country=country
		
	def createNew(self):
		"""This method creates a new entry in the directory.
		The desired file must exists.
		"""
		f= open(pathd,"a+")
		f.write(str(self.name+","+self.email+","+self.age+","+self.country))
		f.write("\n")
		f.close
		return True
	def saveAll(self):
		"""This method saves the directory.
		The desired file must exists.
		It writes the new record and save the file.
		"""
		f= open(pathd,"a+")
		#print self.items
		f.write(str(self.items))
		f.write("\n")
		f.close
		return True

	def delete(self,rec):
		"""This method searches for an entry and deletes it.
		The desired file must exists.
		If the file do not exist it prints the error message.
		"""
		done=False
		it=self.loadFromFile(pathd)
		#print "rec ", rec
		for x in it:
			#print x
			if done==False:
				#print rec," ",len(rec)," ",len(x)
				if rec==x:
					#print "\n",x,"\n"
					done = True
		if done==True:
			with open(pathd, "r") as f:
				lines = f.readlines()
			with open(pathd, "w") as f:
   				for line in lines:
       					if line.strip("\n") != x:
						f.write(line)
		else:
			print "Record not found, ", done


	def loadFromFile(self,filename):
		"""This method load the entries in the directory.
		The desired file must exists.
		If the file do not exist it prints the error message.
		"""
		try:
			with open(filename) as f:
				self.items = f.read().splitlines()
				print self.items
				return self.items
		except IOError:
			print "Error, file does not exist"

	def searchRecord(self,rec):
		"""This method load the entries in the directory.
		The desired file must exists.
		If the file do not exist it prints the error message.
		"""
		done=False
		it=self.loadFromFile(pathd)
		for x in it:
			#for y in x:
				if done==False:
					if rec==x:#y:
						#print "\n",x,"\n"
						done = True
						return done,x


def openFile(filename):
	"""This method opens the file.
    """
	try:
		with open(filename) as f:
			data=f.read()
			return data
	except IOError:
		return "Error, file does not exist"

class TestDirMethods(unittest.TestCase):
	"""This is the class to do unit testing on directory methods.
	"""
	
	def test_fileExist(self):
		"""Testing that file exists.
        """
		filename=pathd
		ret=openFile(filename)
		self.assertNotEqual(ret,"Error, file does not exist")

	def test_fileNotExist(self):
		"""Testing that unkonwn file throws an error message.
		"""
		filename=pathd+'a'
		ret=openFile(filename)
		self.assertEqual(ret,"Error, file does not exist")

	def test_newR1(self):
		"""Testing new entry.
		"""
		compare="a,b,c,d\n"
		it=directory('a','b','c','d')
		it.createNew()
		with open(pathd) as f:
				fline = f.readline()
		f.close
		self.assertEqual(compare,fline)

	def test_somethingInFile(self):
		"""Testing that file contains something.
		"""
		filename=pathd
		ret=openFile(filename)
		self.assertNotEqual(ret,"")

	def test_delRecord(self):
		"""Testing that a record is deleted.
		"""
		compare="a,b,c,d\n"
		with open(pathd) as f:
				fline = f.read()
		f.close
		rec='a','b','c','d'
		it=directory('a','b','c','d')
		it.delete(fline[:7])
		#print fline
		fline2=it.searchRecord(fline)
		self.assertNotEqual(fline,fline2)

	def test_printSomething(self):
		"""Testing that method class print something.
		"""
		it=directory('a','b','c','d')
		o=StringIO.StringIO()
		sys.stdout = o
		it.loadFromFile(pathd)
		sys.stdout = sys.__stdout__
		self.assertNotEqual(o.getvalue(), "")

	def test_printFile(self):
		"""Testing that method class prints the entries.
		"""
		it=directory('a','b','c','d')
		ret=openFile(pathd)
		o=StringIO.StringIO()
		sys.stdout = o
		it.loadFromFile(pathd)
		sys.stdout = sys.__stdout__
		self.assertNotEqual(o.getvalue(), ret)

if __name__ == '__main__':
    unittest.main()
