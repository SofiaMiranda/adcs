import unittest


class SortDataLists:

    def __init__(self, path, array):
        self.path = path
        self.array = array

    def set_input_data(self):
        slist = []
        try:
            with open(self.path) as f:
                lines = f.read().splitlines()
                for line in lines:
                    slist.append(line)
                return slist
                # data = f.read()
                # return data
        except IOError:
            return "Error, file does not exist"

    def set_output_data(self):
        try:
            with open(self.path) as f:
                data = f.read()
                return data
        except IOError:
            return "Error, file does not exist"

    def execute_merge_sort(self):
        i = 0
        j = 0
        k = 0

        if len(self.array) > 1:
            mid = len(self.array) // 2
            left = self.array[:mid]
            right = self.array[mid:]

            leftsort = SortDataLists("", left)
            leftsort.execute_merge_sort()
            rightsort = SortDataLists("", right)
            rightsort.execute_merge_sort()

            while i < len(left) and j < len(right):
                if left[i] < right[j]:
                    self.array[k] = left[i]
                    i += 1
                else:
                    self.array[k] = right[j]
                    j += 1
                k += 1

            while i < len(left):
                self.array[k] = left[i]
                i += 1
                k += 1

            while j < len(right):
                self.array[k] = right[j]
                j += 1
                k += 1
        return self.array


it = SortDataLists("data.csv", "")
ret = it.set_input_data()

it2 = SortDataLists("data.csv", ret)
ret3 = it2.execute_merge_sort()


class TestSortMethods(unittest.TestCase):

    def test_fileInputExist(self):
        filename = "data.csv"
        t = SortDataLists(filename, "")
        ret = t.set_input_data()
        self.assertNotEqual(ret, "Error, file does not exist")

    def test_fileInputNotExist(self):
        filename = "data.csv"
        filename = filename + 'a'
        t = SortDataLists(filename, "")
        ret = t.set_input_data()
        self.assertEqual(ret, "Error, file does not exist")

    def test_fileOutputExist(self):
        filename = "data.csv"
        t = SortDataLists(filename, "")
        ret = t.set_output_data()
        self.assertNotEqual(ret, "Error, file does not exist")

    def test_fileOutputNotExist(self):
        filename = "data.csv"
        filename = filename + 'a'
        t = SortDataLists(filename, "")
        ret = t.set_output_data()
        self.assertEqual(ret, "Error, file does not exist")

    def test_returnSomethingMergeSort(self):
        it = SortDataLists("data.csv", "")
        ret = it.set_input_data()
        it2 = SortDataLists("data.csv", ret)
        ret3 = it2.execute_merge_sort()
        self.assertNotEqual(ret3, "")


if __name__ == '__main__':
    unittest.main()
