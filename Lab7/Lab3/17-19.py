import unittest
import math
import filecmp



def filefunc():
	return filecmp.cmp("f1.txt", "f2.txt", shallow=True)


class TestMathMethods(unittest.TestCase):
	def test_ceil1(self):
		self.assertEqual(math.ceil(4.5),5)

	@unittest.expectedFailure
	def test_ceilFail(self):
		self.assertEqual(math.ceil(4.5),4)

	def test_ceilNotANumber(self):
		with self.assertRaises(TypeError):math.ceil('a')

	def test_factorial1(self):
		self.assertEqual(math.factorial(3),6)

	@unittest.expectedFailure
	def test_factFail(self):
		self.assertEqual(math.factorial(3),5)

	def test_factNotANumber(self):
		with self.assertRaises(TypeError):math.factorial('a')

	def test_factNegative(self):
		with self.assertRaises(ValueError):math.factorial(-1)


	def test_pow1(self):
		self.assertEqual([math.pow(n,2) for n in range(7)], [0,1, 4, 9, 16, 25,36])

	@unittest.expectedFailure
	def test_pow2(self):
		self.assertEqual([math.pow(n,2) for n in range(7)], [1,1, 4, 9, 16, 25,36])

	def test_powNotANumber(self):
		with self.assertRaises(TypeError):math.pow('a')

	def test_pow4(self):
		with self.assertRaises(TypeError):math.pow(5)
	
	def test_file1(self):
		self.assertTrue(filecmp.cmp("f1.txt", "f2.txt", shallow=True))

	def test_fileInexistent(self):
		with self.assertRaises(OSError):filecmp.cmp("f3.txt", "f2.txt", shallow=True)
	
	@unittest.expectedFailure	
	def test_file3(self):
		self.assertTrue(filecmp.cmp("f1.txt", "f4.txt", shallow=True))


if __name__ == '__main__':
    unittest.main()


