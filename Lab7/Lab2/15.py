class directory:
	item=[]
	items=[]
		
	def __init__(self,name,address,phone,email):
		self.name=name
		self.address=address
		self.phone=phone
		self.email=email
		
	def createNew(self):	
		self.items.append([self.name,self.address,self.phone,self.email])

	def saveAll(self):
		f= open("directory.txt","w+")
		print self.items
		f.write(str(self.items))
		f.close

	def loadFromFile(self,filename):
		try:
			with open(filename) as f:
				self.items = f.read().splitlines()				
		except IOError:
			print "Error, file does not exist"	

	def searchRecord(self,rec):
		done=False
		for x in self.items:
			for y in x:	
				if done==False:		
					if rec==y:
						print "\n",x,"\n"	
						done = True				
						return done
					

itemlist1=directory("name1","address1","3333333333","name1@outlook.com")
itemlist2=directory("name2","address2","2222222222","name2@outlook.com")
itemlist1.createNew()
itemlist2.createNew()
while True:
	action=raw_input("\n1. Create new record\n2. Save all records in a file.\n3. Load record from file.\n4. Search a record.\n5. Exit.\nSelect action:\n")
	if action=="1":
		name=raw_input("name: ") 
		address=raw_input("address: ") 
		phone=raw_input("phone: ") 
		email=raw_input("email: ") 	
		itemlist=directory(name,address,phone,email)		
		itemlist.createNew()	
		print "\n",itemlist.items,"\n"
	elif action=="2":
		itemlist.saveAll()
		print "\n","Saved in directory.txt","\n"
	elif action=="3":
		filename=raw_input("Filename: ")
		itemlist.loadFromFile(filename)
		print itemlist.items
	elif action=="4":
		itemlist=directory(name,address,phone,email)
		rec=raw_input("Record to find: ")
		done=itemlist.searchRecord(rec)
		if done!=True:
			print "\nRecord not found.\n"
	elif action=="5":
		print "exiting"
		break
	else:
		print action, " is not an option"